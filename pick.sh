#!/bin/bash

#
# Allows picking single commits from the enterprise repository. Unlike normal cherry picking, this command will however discard any changes which
# are not Apache 2 licensed. This is achieved by a whitelist of directories.
#
# In order to use this command, the enterprise repo needs to be set up as remote. You can do that this way:
# 
# $ git remote add enterprise https://git.floragunn.com/search-guard/search-guard-suite-enterprise.git
# $ git fetch enterprise
#
# Use this command this way:
#
# ./pick.sh 229d7819cf32d1a625cb6aa7298904ac3f67ea8c

set -e -u

# Make sure there are no other staged files
git reset --mixed

# Create a patch containing the single commit in $1. The patch will only contain the files matching the pathes specified below
patch=$(git diff $1^ $1 dev plugin scheduler security signals support sgadmin codova .gitignore pom.xml setup_test_instance.sh)

# Apply the patch
git apply --reject --whitespace=fix --index - <<< "$patch"

# Commit the patch using the author and commit message of the original commit
git commit --reuse-message=$1
