package com.floragunn.searchguard.sgconf;

public interface Hashed {
    
    String getHash();
    void clearHash();

}
